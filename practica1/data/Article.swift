//
//  Articles.swift
//  practica1
//
//  Created by Raul Rodriguez Concepcion on 09/03/2019.
//  Copyright © 2019 Raul Rodriguez Concepcion. All rights reserved.
//

import Foundation

struct Article {
    
    let name: String
    let price: Double
    
    init(name: String, price: Double) {
        self.name = name
        self.price = price
    }
}
