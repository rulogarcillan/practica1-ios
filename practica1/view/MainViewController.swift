//
//  MainViewController.swift
//  practica1
//
//  Created by Raul Rodriguez Concepcion on 09/03/2019.
//  Copyright © 2019 Raul Rodriguez Concepcion. All rights reserved.
//

import UIKit

class MainViewController: UIViewController {
    
    @IBOutlet weak var tableView: UITableView!
    @IBOutlet weak var totalPriceLB: UILabel!
    var shoppingList : [Article] = []

    override func viewDidLoad() {
        super.viewDidLoad()
        
        shoppingList.append(Article(name: "Orange", price: 1.85))
        shoppingList.append(Article(name: "Apple", price: 1.20))
        shoppingList.append(Article(name: "Pear", price: 1.69))
        shoppingList.append(Article(name: "Carrot", price: 2))
        shoppingList.append(Article(name: "Spinach", price: 3.25))
        shoppingList.append(Article(name: "Milk", price: 0.59))
        
        totalPriceLB.text = String(getAmountShoppingList()) + " €"
        
        tableView.register(UITableViewCell.self, forCellReuseIdentifier: "idCellArticle")
       
    }
    
    
    func getAmountShoppingList() -> Double {
        return shoppingList.reduce(0) { (result, next) -> Double in
            return result + next.price
        }
    }

}


extension MainViewController : UITableViewDataSource {
    
    func tableView(_ tableView: UITableView, numberOfRowsInSection section: Int) -> Int {
        return shoppingList.count
    }
    
    func tableView(_ tableView: UITableView, cellForRowAt indexPath: IndexPath) -> UITableViewCell {
        
        
        let item = tableView.dequeueReusableCell(withIdentifier: "idCellArticle")
        item?.textLabel?.text = shoppingList[indexPath.item].name
        item?.detailTextLabel?.text = String(shoppingList[indexPath.item].price) + " €"
        
        //sin esto no se muestra el detalle del label, ¿Por que?
        let label = UILabel.init(frame: CGRect(x:0,y:0,width:100,height:20))
        label.text = String(shoppingList[indexPath.item].price) + " €"
        item?.accessoryView = label
        
        return item ?? UITableViewCell()
    }
}

extension MainViewController: UITableViewDelegate{
    
}
