//
//  ExitViewController.swift
//  practica1
//
//  Created by Raul Rodriguez Concepcion on 09/03/2019.
//  Copyright © 2019 Raul Rodriguez Concepcion. All rights reserved.
//

import UIKit

class ExitViewController: UIViewController {

    @IBOutlet weak var exitLB: UILabel!
    override func viewDidLoad() {
        super.viewDidLoad()
    }
    

    override func viewDidAppear(_ animated: Bool) {
        super.viewDidAppear(animated)
        
        UIView.animate(withDuration: 1, animations: {
            self.exitLB.frame.origin = CGPoint(x: self.exitLB.frame.origin.x, y: self.view.bounds.height)
        }) { (true) in
            exit(0);
        }
        
    }

}
